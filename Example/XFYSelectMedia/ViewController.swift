//
//  ViewController.swift
//  XFYSelectMedia
//
//  Created by leonazhu on 03/29/2019.
//  Copyright (c) 2019 leonazhu. All rights reserved.
//

import UIKit
import XFYSelectMedia

class ViewController: UIViewController {

    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var textView: UITextView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
    }
    
    // MARK: 操作
    @IBAction func selectPhoto(_ sender: Any) {
        presentPhotoPicker(isCamera: false) { [weak self] info in
            guard let `self` = self, let image = info[.originalImage] as? UIImage else { return }
            self.imageView.image = image
            self.textView.text = "\(info)"
        }
    }
    @IBAction func selectPhotoAllowsEdit(_ sender: Any) {
        let photoPicker = presentPhotoPicker(isCamera: false) { [weak self] info in
            guard let `self` = self, let image = info[.editedImage] as? UIImage else { return }
            self.imageView.image = image
            self.textView.text = "\(info)"
        }
        photoPicker.allowsEditing = true
    }
    @IBAction func selectMovie(_ sender: Any) {
        let moviePicker = presentMoviePicker(isCamera: false) { [weak self] info in
            guard let `self` = self else { return }
            self.textView.text = "\(info)"
        }
        moviePicker.allowsEditing = true
        moviePicker.videoMaximumDuration = 5
    }
    @IBAction func makePhoto(_ sender: Any) {
        guard UIImagePickerController.isSourceTypeAvailable(.camera) else {
            print("不支持拍照")
            return
        }
        presentPhotoPicker(isCamera: true) { [weak self] info in
            guard let `self` = self, let image = info[.originalImage] as? UIImage else { return }
            self.imageView.image = image
            self.textView.text = "\(info)"
        }
    }
    @IBAction func makeMovie(_ sender: Any) {
        guard UIImagePickerController.isSourceTypeAvailable(.camera) else {
            print("不支持录像")
            return
        }
        let moviePicker = presentMoviePicker(isCamera: true) { [weak self] info in
            guard let `self` = self else { return }
            self.textView.text = "\(info)"
        }
        moviePicker.allowsEditing = true
        moviePicker.videoMaximumDuration = 5
        present(moviePicker, animated: true, completion: nil)
    }
    @IBAction func selectMultPhoto(_ sender: Any) {
        presentImagePicker(maxSelected: 3) { phAssets in
            self.textView.text = "\(phAssets)"
        }
    }
}
