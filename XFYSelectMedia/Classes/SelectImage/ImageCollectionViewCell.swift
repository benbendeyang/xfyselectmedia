//
//  ImageCollectionViewCell.swift
//  Pods-XFYSelectMedia_Example
//
//  Created by 🐑 on 2019/3/30.
//

import UIKit

class ImageCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var selectButton: UIButton!
    
    override var isSelected: Bool {
        didSet {
            selectButton.isSelected = isSelected
        }
    }
    
    func playAnimate() {
        UIView.animateKeyframes(withDuration: 0.4, delay: 0, options: .allowUserInteraction, animations: {
            UIView.addKeyframe(withRelativeStartTime: 0, relativeDuration: 0.2, animations: { [weak self] in
                self?.selectButton.transform = CGAffineTransform(scaleX: 0.7, y: 0.7)
            })
            UIView.addKeyframe(withRelativeStartTime: 0.2, relativeDuration: 0.4, animations: { [weak self] in
                self?.selectButton.transform = CGAffineTransform.identity
        })
        }, completion: nil)
    }
}
