//
//  SelectImageNavigationController.swift
//  Pods-XFYSelectMedia_Example
//
//  Created by 🐑 on 2019/3/30.
//

import UIKit
import Photos

open class SelectImageNavigationController: UINavigationController {
    
    /// 每次最多可选择的照片数量
    var maxSelected: Int = .max
    
    /// 照片选择完毕后的回调
    var completeHandler: ((_ assets: [PHAsset]) -> ())?
    var statusBarStyle: UIStatusBarStyle = UIApplication.shared.statusBarStyle
    
    override open var preferredStatusBarStyle: UIStatusBarStyle {
        return statusBarStyle
    }
    
    override open func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
}
