//
//  ImagePickerController.swift
//  Pods-XFYSelectMedia_Example
//
//  Created by 🐑 on 2019/3/30.
//

import UIKit
import Photos

/// 相簿列表项
struct ImageAlbumItem {
    
    /// 相簿名称
    var title: String?
    /// 相簿内的资源
    var fetchResult: PHFetchResult<PHAsset>
}

class ImagePickerController: UIViewController {

    @IBOutlet weak var mainTableView: UITableView!
    
    /// 相簿列表项集合
    var items: [ImageAlbumItem] = []
    
    /// 带缓存的图片管理对象
    let imageManager = PHCachingImageManager()
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        // 申请权限
        PHPhotoLibrary.requestAuthorization { [weak self] status in
            guard status == .authorized, let `self` = self else { return }
            // 列出所有系统的智能相册
            let smartAlbums = PHAssetCollection.fetchAssetCollections(with: .smartAlbum, subtype: .albumRegular, options: PHFetchOptions())
            self.convertCollection(collection: smartAlbums)
            // 列出所有用户创建的相册
            if let userCollections = PHAssetCollection.fetchTopLevelUserCollections(with: nil) as? PHFetchResult<PHAssetCollection> {
                self.convertCollection(collection: userCollections)
            }
            // 异步加载，需要在主线程中刷新UI
            DispatchQueue.main.async { [weak self] in
                guard let `self` = self else { return }
                self.mainTableView.reloadData()
                // 首次进入直接打开第一个相册图片展示页
                // Todo: 展示
                if let controller = self.storyboard?.instantiateViewController(withIdentifier: "ImageCollectionViewController") as? ImageCollectionViewController {
                    controller.title = self.items.first?.title
                    controller.assetsFetchResults = self.items.first?.fetchResult
                    self.navigationController?.pushViewController(controller, animated: false)
                }
            }
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        initView()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "ShowImages" {
            guard let controller = segue.destination as? ImageCollectionViewController,
                let cell = sender as? ImagePickerCell,
                let indexPath = mainTableView.indexPath(for: cell) else {
                    return
            }
            let item = items[indexPath.row]
            controller.title = item.title
            controller.assetsFetchResults = item.fetchResult
        }
    }
    
    // MARK: 操作
    @IBAction func cancel(_ sender: Any) {
        dismiss(animated: true)
    }
}

// MARK: - 公共
extension ImagePickerController {
    
}

// MARK: - 私有
private extension ImagePickerController {
    
    func initView() {
        imageManager.stopCachingImagesForAllAssets()
    }
    
    /// 转化处理获取到的相簿
    func convertCollection(collection: PHFetchResult<PHAssetCollection>) {
        for i in 0..<collection.count {
            // 获取当前相簿内的图片
            let resultsOptions = PHFetchOptions()
            resultsOptions.sortDescriptors = [NSSortDescriptor(key: "creationDate", ascending: false)]
            resultsOptions.predicate = NSPredicate(format: "mediaType = %d", PHAssetMediaType.image.rawValue)
            let item = collection[i]
            let assetsFetchResult = PHAsset.fetchAssets(in: item, options: resultsOptions)
            // 没有图片的空相簿不显示
            if assetsFetchResult.count > 0 {
                let title = titleOfAlbumForChinse(title: item.localizedTitle)
                items.append(ImageAlbumItem(title: title, fetchResult: assetsFetchResult))
            }
        }
    }
    
    /// 由于系统返回的相册集名称为英文，我们需要转换为中文
    func titleOfAlbumForChinse(title:String?) -> String? {
        if title == "Slo-mo" {
            return "慢动作"
        } else if title == "Recently Added" {
            return "最近添加"
        } else if title == "Favorites" {
            return "个人收藏"
        } else if title == "Recently Deleted" {
            return "最近删除"
        } else if title == "Videos" {
            return "视频"
        } else if title == "All Photos" {
            return "所有照片"
        } else if title == "Selfies" {
            return "自拍"
        } else if title == "Screenshots" {
            return "屏幕快照"
        } else if title == "Camera Roll" {
            return "相机胶卷"
        }
        return title
    }
}

// MARK: - 协议
// MARK: 列表
extension ImagePickerController: UITableViewDelegate, UITableViewDataSource {

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return items.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "ImagePickerCell", for: indexPath)
            as! ImagePickerCell
        let item = items[indexPath.row]
        cell.titleLabel.text = item.title
        cell.countLabel.text = "（\(item.fetchResult.count)）"
        if let asset = item.fetchResult.firstObject {
            imageManager.requestImage(for: asset, targetSize: CGSize(width: 240, height: 240), contentMode: .aspectFit, options: nil) { [weak cell] image, _ in
                cell?.coverImageView.image = image
            }
        }
        return cell
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 80
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
    }
}
