//
//  ImageCompleteButton.swift
//  Pods-XFYSelectMedia_Example
//
//  Created by 🐑 on 2019/3/30.
//

import UIKit

class ImageCompleteButton: UIView {

    private var numberLabel = UILabel()
    var titleLabel = UILabel()
    
    let defaultFrame = CGRect(x: 0, y: 0, width: 70, height: 20)
    
    let titleColor = #colorLiteral(red: 0.03529411765, green: 0.7333333333, blue: 0.02745098039, alpha: 1)
    
    lazy var tapSingle: UITapGestureRecognizer = {
        let tap = UITapGestureRecognizer()
        tap.numberOfTapsRequired = 1
        tap.numberOfTouchesRequired = 1
        addGestureRecognizer(tap)
        return tap
    }()

    var number: Int = 0 {
        didSet {
            if number == 0 {
                numberLabel.isHidden = true
                isEnabled = false
            } else {
                numberLabel.isHidden = false
                numberLabel.text = "\(number)"
                playAnimate()
                isEnabled = true
            }
        }
    }
    
    var isEnabled: Bool = true {
        didSet {
            if isEnabled {
                titleLabel.textColor = titleColor
                tapSingle.isEnabled = true
            } else {
                titleLabel.textColor = .gray
                tapSingle.isEnabled = false
            }
        }
    }
    
    init() {
        super.init(frame:defaultFrame)
        
        //已选照片数量标签初始化
        numberLabel.frame = CGRect(x: 0 , y: 0 , width: 20, height: 20)
        numberLabel.backgroundColor = titleColor
        numberLabel.layer.cornerRadius = 10
        numberLabel.layer.masksToBounds = true
        numberLabel.textAlignment = .center
        numberLabel.font = UIFont.systemFont(ofSize: 15)
        numberLabel.textColor = .white
        numberLabel.isHidden = true
        addSubview(numberLabel)
        
        //按钮标题标签初始化
        titleLabel = UILabel(frame:CGRect(x: 20 , y: 0 ,
                                          width: defaultFrame.width - 20,
                                          height: 20))
        titleLabel.text = "完成"
        titleLabel.textAlignment = .center
        titleLabel.font = UIFont.systemFont(ofSize: 15)
        titleLabel.textColor = titleColor
        addSubview(titleLabel)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    // MARK: 公共
    /// 添加事件响应
    func addTarget(target: Any, action: Selector) {
        tapSingle.addTarget(target, action: action)
    }
    
    // MARK: 私有
    /// 用户数字改变时播放的动画
    private func playAnimate() {
        // 从小变大，且有弹性效果
        numberLabel.transform = CGAffineTransform(scaleX: 0.1, y: 0.1)
        UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 0.5, initialSpringVelocity: 0.5, options: UIView.AnimationOptions(), animations: { [weak self] in
            self?.numberLabel.transform = CGAffineTransform.identity
        }, completion: nil)
    }
    
}
