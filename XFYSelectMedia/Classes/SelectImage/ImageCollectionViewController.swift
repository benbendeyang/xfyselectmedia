//
//  ImageCollectionViewController.swift
//  Pods-XFYSelectMedia_Example
//
//  Created by 🐑 on 2019/3/30.
//

import UIKit
import Photos

class ImageCollectionViewController: UIViewController {

    @IBOutlet weak var mainCollectionView: UICollectionView!
    @IBOutlet weak var toolBar: UIToolbar!
    
    var assetsFetchResults: PHFetchResult<PHAsset>?
    let imageManager = PHCachingImageManager()
    let completeButton = ImageCompleteButton()
    var assetGridThumbnailSize: CGSize = CGSize(width: 200, height: 200)
    
    var selectedCount: Int {
        return mainCollectionView.indexPathsForSelectedItems?.count ?? 0
    }
    
    lazy var maxSelected: Int = {
        guard let navigationController = self.navigationController as? SelectImageNavigationController else { return 0 }
        return navigationController.maxSelected
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        initView()
    }
    
    // MARK: 操作
    @IBAction func cancel(_ sender: Any) {
        dismiss(animated: true)
    }
}

// MARK: - 私有
private extension ImageCollectionViewController {
    
    func initView() {
        imageManager.stopCachingImagesForAllAssets()
        
        if let layout = mainCollectionView.collectionViewLayout as? UICollectionViewFlowLayout {
            let length = (UIScreen.main.bounds.width - 4 * 5) / 4
            layout.itemSize = CGSize(width: length, height: length)
            let scale = UIScreen.main.scale
            assetGridThumbnailSize = CGSize(width: scale * length, height: scale * length)
        }
        mainCollectionView.allowsMultipleSelection = true
        
        completeButton.center = CGPoint(x: UIScreen.main.bounds.width - 50, y: toolBar.bounds.height / 2)
        completeButton.number = 0
        completeButton.addTarget(target: self, action: #selector(finishSelect))
        toolBar.addSubview(completeButton)
    }

    @objc func finishSelect() {
        guard let assetsFetchResults = assetsFetchResults,
            let navigationController = self.navigationController as? SelectImageNavigationController else {
            return
        }
        //取出已选择的图片资源
        var assets: [PHAsset] = []
        if let indexPaths = mainCollectionView.indexPathsForSelectedItems {
            for indexPath in indexPaths {
                assets.append(assetsFetchResults[indexPath.row])
            }
        }
        //调用回调函数
        navigationController.dismiss(animated: true, completion: {
            navigationController.completeHandler?(assets)
        })
    }
}

// MARK: - 协议
// MARK: UICollectionView
extension ImageCollectionViewController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return assetsFetchResults?.count ?? 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ImageCollectionViewCell", for: indexPath) as! ImageCollectionViewCell
        if let results = assetsFetchResults, indexPath.row < results.count {
            let asset = results[indexPath.row]
            imageManager.requestImage(for: asset, targetSize: assetGridThumbnailSize, contentMode: .aspectFit, options: nil) { [weak cell] image, _ in
                cell?.imageView.image = image
            }
        }
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        guard let cell = collectionView.cellForItem(at: indexPath) as? ImageCollectionViewCell else { return }
        let count = selectedCount
        if count > maxSelected {
            collectionView.deselectItem(at: indexPath, animated: false)
            //弹出提示
            let title = "你最多只能选择\(maxSelected)张照片"
            let alertController = UIAlertController(title: title, message: nil, preferredStyle: .alert)
            let cancelAction = UIAlertAction(title:"我知道了", style: .cancel, handler:nil)
            alertController.addAction(cancelAction)
            present(alertController, animated: true, completion: nil)
        } else {
            completeButton.number = count
            cell.playAnimate()
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didDeselectItemAt indexPath: IndexPath) {
        guard let cell = collectionView.cellForItem(at: indexPath) as? ImageCollectionViewCell else { return }
        completeButton.number = selectedCount
        cell.playAnimate()
    }
}
