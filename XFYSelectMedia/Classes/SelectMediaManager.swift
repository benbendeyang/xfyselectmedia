//
//  SelectMediaManager.swift
//  Pods-XFYSelectMedia_Example
//
//  Created by 🐑 on 2019/4/1.
//

import Foundation
import Photos

open class SelectMediaManager {
    
    public var navigationStyle: (tinitColor: UIColor, barTintColor: UIColor, titleFont: UIFont)?
    public var statusBarStyle: UIStatusBarStyle?
    
    public static let shared = SelectMediaManager()
    
    public func setStyle(navigationStyle: (tinitColor: UIColor, barTintColor: UIColor, titleFont: UIFont)?, statusBarStyle: UIStatusBarStyle? = nil) {
        self.navigationStyle = navigationStyle
        self.statusBarStyle = statusBarStyle
    }
}

public extension UIViewController {
    
    /// 选择图片
    @discardableResult
    func presentPhotoPicker(using managerStyle: Bool = true, isCamera: Bool, selectFinishBlock: ((_ mediaInfo: [UIImagePickerController.InfoKey : Any]) -> Void)?) -> MediaPickerController {
        let photoPicker = MediaPickerController()
        photoPicker.sourceType = isCamera ? .camera : .photoLibrary
        if managerStyle {
            if let navigationStyle = SelectMediaManager.shared.navigationStyle {
                photoPicker.navigationBar.tintColor = navigationStyle.tinitColor
                photoPicker.navigationBar.barTintColor = navigationStyle.barTintColor
                photoPicker.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: navigationStyle.tinitColor, NSAttributedString.Key.font: navigationStyle.titleFont]
            }
            if let statusBarStyle = SelectMediaManager.shared.statusBarStyle {
                photoPicker.statusBarStyle = statusBarStyle
            }
        }
        photoPicker.selectFinishBlock = selectFinishBlock
        present(photoPicker, animated: true, completion: nil)
        return photoPicker
    }
    
    /// 选择视频
    @discardableResult
    func presentMoviePicker(using managerStyle: Bool = true, isCamera: Bool, selectFinishBlock: ((_ mediaInfo: [UIImagePickerController.InfoKey : Any]) -> Void)?) -> MediaPickerController {
        
        let moviePicker =  MediaPickerController()
        moviePicker.sourceType = isCamera ? .camera : .photoLibrary
        moviePicker.mediaTypes = [kUTTypeMovie as String]
        if managerStyle {
            if let navigationStyle = SelectMediaManager.shared.navigationStyle {
                moviePicker.navigationBar.tintColor = navigationStyle.tinitColor
                moviePicker.navigationBar.barTintColor = navigationStyle.barTintColor
                moviePicker.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: navigationStyle.tinitColor, NSAttributedString.Key.font: navigationStyle.titleFont]
            }
            if let statusBarStyle = SelectMediaManager.shared.statusBarStyle {
                moviePicker.statusBarStyle = statusBarStyle
            }
        }
        moviePicker.selectFinishBlock = selectFinishBlock
        present(moviePicker, animated: true, completion: nil)
        return moviePicker
    }
    
    /// ImagePicker提供给外部调用的接口，同于显示图片选择页面
    @discardableResult
    func presentImagePicker(maxSelected: Int = .max, using managerStyle: Bool = true, completeHandler: ((_ assets: [PHAsset])->())?)
        -> SelectImageNavigationController? {
            //获取图片选择视图控制器
            guard let url = Bundle(for: SelectImageNavigationController.self).url(forResource: "SelectImage", withExtension: "bundle"), let bundle = Bundle(url: url) else {
                return nil
            }
            guard let navigationController = UIStoryboard(name: "SelectImage", bundle: bundle).instantiateInitialViewController() as? SelectImageNavigationController else {
                return nil
            }
            navigationController.maxSelected = maxSelected
            navigationController.completeHandler = completeHandler
            if managerStyle {
                if let navigationStyle = SelectMediaManager.shared.navigationStyle {
                    navigationController.navigationBar.tintColor = navigationStyle.tinitColor
                    navigationController.navigationBar.barTintColor = navigationStyle.barTintColor
                    navigationController.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: navigationStyle.tinitColor, NSAttributedString.Key.font: navigationStyle.titleFont]
                }
                if let statusBarStyle = SelectMediaManager.shared.statusBarStyle {
                    navigationController.statusBarStyle = statusBarStyle
                }
            }
            present(navigationController, animated: true)
            return navigationController
    }
}

