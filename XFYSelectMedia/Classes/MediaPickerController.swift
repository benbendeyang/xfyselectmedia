//
//  MediaPickerController.swift
//  Pods-XFYSelectMedia_Example
//
//  Created by 🐑 on 2019/3/29.
//

import UIKit
@_exported import MobileCoreServices
@_exported import AssetsLibrary

open class MediaPickerController: UIImagePickerController {
    
    public var selectFinishBlock: ((_ mediaInfo: [UIImagePickerController.InfoKey : Any]) -> Void)?
    public var statusBarStyle: UIStatusBarStyle = UIApplication.shared.statusBarStyle
    
    override open var preferredStatusBarStyle: UIStatusBarStyle {
        return statusBarStyle
    }
    
    override open func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        delegate = self
    }
}

extension MediaPickerController: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    public func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        picker.dismiss(animated: true) { [weak self] in
            self?.selectFinishBlock?(info)
        }
    }
}
