# XFYSelectMedia

[![CI Status](https://img.shields.io/travis/leonazhu/XFYSelectMedia.svg?style=flat)](https://travis-ci.org/leonazhu/XFYSelectMedia)
[![Version](https://img.shields.io/cocoapods/v/XFYSelectMedia.svg?style=flat)](https://cocoapods.org/pods/XFYSelectMedia)
[![License](https://img.shields.io/cocoapods/l/XFYSelectMedia.svg?style=flat)](https://cocoapods.org/pods/XFYSelectMedia)
[![Platform](https://img.shields.io/cocoapods/p/XFYSelectMedia.svg?style=flat)](https://cocoapods.org/pods/XFYSelectMedia)

## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

XFYSelectMedia is available through [CocoaPods](https://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod 'XFYSelectMedia'
```

## Author

leonazhu, 412038671@qq.com

## License

XFYSelectMedia is available under the MIT license. See the LICENSE file for more info.
