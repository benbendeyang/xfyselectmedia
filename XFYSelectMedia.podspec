#
# Be sure to run `pod lib lint XFYSelectMedia.podspec' to ensure this is a
# valid spec before submitting.
#
# Any lines starting with a # are optional, but their use is encouraged
# To learn more about a Podspec see https://guides.cocoapods.org/syntax/podspec.html
#

Pod::Spec.new do |s|
  s.name             = 'XFYSelectMedia'
  s.version          = '0.2.0'
  s.summary          = 'A short description of XFYSelectMedia.'
  s.swift_version    = '4.2'

# This description is used to generate tags and improve search results.
#   * Think: What does it do? Why did you write it? What is the focus?
#   * Try to keep it short, snappy and to the point.
#   * Write the description between the DESC delimiters below.
#   * Finally, don't worry about the indent, CocoaPods strips it!

  s.description      = <<-DESC
TODO: Add long description of the pod here.
                       DESC

  s.homepage         = 'https://gitlab.com/benbendeyang/xfyselectmedia'
  # s.screenshots     = 'www.example.com/screenshots_1', 'www.example.com/screenshots_2'
  s.license          = { :type => 'MIT', :file => 'LICENSE' }
  s.author           = { 'yang' => '412038671@qq.com' }
  s.source           = { :git => 'https://gitlab.com/benbendeyang/xfyselectmedia.git', :tag => s.version.to_s }
  # s.social_media_url = 'https://twitter.com/<TWITTER_USERNAME>'

  s.ios.deployment_target = '10.0'

  s.source_files = 'XFYSelectMedia/Classes/**/*'
  
  s.subspec 'SelectImage' do |a|
    a.source_files = 'XFYSelectMedia/Classes/SelectImage/*'
    a.resource_bundles = {
      'SelectImage' => ['XFYSelectMedia/Assets/SelectImage/*.{storyboard,xcassets}']
    }
  end
    
#  s.resource_bundles = {
#    'XFYSelectMedia' => ['XFYSelectMedia/Assets/SelectImage/*.{storyboard}']
#  }
  # s.resource_bundles = {
  #   'XFYSelectMedia' => ['XFYSelectMedia/Assets/*.png']
  # }

  # s.public_header_files = 'Pod/Classes/**/*.h'
  # s.frameworks = 'UIKit', 'MapKit'
  # s.dependency 'AFNetworking', '~> 2.3'
end
